package com.example.hibernateEmpExample;

import com.example.hibernateEmpExample.components.employeetable;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.util.List;
import java.util.Scanner;

@SpringBootApplication
public class HibernateEmpExampleApplication {


	public static void main(String[] args) {

		String ConfigrationFile = "hibernate1.cfg.xml";
		ClassLoader classLoaderObj = HibernateEmpExampleApplication.class.getClassLoader();
		File f = new File(classLoaderObj.getResource(ConfigrationFile).getFile());
		SessionFactory sessionFactoryObj = new Configuration().configure(f).buildSessionFactory();
		Session sessionObj = sessionFactoryObj.openSession();

		System.out.println("enter the operation you want to perform");
		System.out.println("Enter 1 for save records");
		System.out.println("Enter 2 for update records");
		System.out.println("Enter 3 for delete records");
		System.out.println("Enter 4 for display records");
		Scanner scanner = new Scanner(System.in);
		int input = scanner.nextInt();
		if (input ==1){
			saveRecord(sessionObj);
		} else if (input==2) {
			update(sessionObj);
		} else if (input==3) {
			delete(sessionObj);
		} else if (input==4) {
			display(sessionObj);
		}
		else {
			System.out.println("Enter a valid input");
		}



	}

	public static void display(Session sessionObj){
		Query queryObj = sessionObj.createQuery("FROM employeetable");
		List<employeetable> listObj = queryObj.list();
		for (employeetable iterate: listObj){
			System.out.println(iterate.getEid()+" "+ iterate.getEname()+" "+ iterate.getSal()+" "+ iterate.getDept());
		}
	}
	public static void update(Session sessionObj){
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the eid(primary key) to update the records");
		int eid = scanner.nextInt();
		employeetable employeetableObj = (com.example.hibernateEmpExample.components.employeetable) sessionObj.get(employeetable.class, eid);
		System.out.println("enter 1 to update ename");
		System.out.println("Enter 2 to update sal");
		System.out.println("Enter 3 to update dept");
		System.out.println("Enter 4 to update both");
		int update = scanner.nextInt();
		if(update==1){
			System.out.println("enter name to update");
			String name = scanner.next();
			employeetableObj.setEname(name);
		} else if (update==2) {
			System.out.println("enter the sal to update");
			int sal = scanner.nextInt();
			employeetableObj.setSal(sal);
		} else if (update==3) {
			System.out.println("enter the dept to update");
			String dept = scanner.next();
			employeetableObj.setDept(dept);
		} else if (update==4) {
			System.out.println("enter name to update");
			String name = scanner.nextLine();
			employeetableObj.setEname(name);
			System.out.println("enter the sal to update");
			int sal = scanner.nextInt();
			employeetableObj.setSal(sal);
			System.out.println("enter the dept to update");
			String dept = scanner.nextLine();
			employeetableObj.setDept(dept);
		}
		sessionObj.beginTransaction();
		sessionObj.saveOrUpdate(employeetableObj);
		sessionObj.getTransaction().commit();
		System.out.println("updated record");
	}
	public static void delete(Session sessionObj){
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the eid (primary key) to delete the record");
		int eid=scanner.nextInt();
		employeetable employeetableObj = (employeetable) sessionObj.get(employeetable.class,eid);
		sessionObj.beginTransaction();
		sessionObj.delete(employeetableObj);
		sessionObj.getTransaction().commit();
		System.out.println("deleted record");
	}

	private static void saveRecord(Session sessionObj) {
		System.out.println("******Welcome to Save Records******");
		Scanner scanner = new Scanner(System.in);
		employeetable employeetableObj = new employeetable();
		System.out.println("Enter the eid");
		int eid= scanner.nextInt();
		employeetableObj.setEid(eid);
		System.out.println("Enter the name");
		String name = scanner.next();
		employeetableObj.setEname(name);
		System.out.println("Enter the salary");
		int sal = scanner.nextInt();
		employeetableObj.setSal(sal);
		System.out.println("Enter the department");
		String dept= scanner.next();
		employeetableObj.setDept(dept);
		sessionObj.beginTransaction();
		sessionObj.save(employeetableObj);
		sessionObj.getTransaction().commit();
		System.out.println("records added into database");

	}



}
