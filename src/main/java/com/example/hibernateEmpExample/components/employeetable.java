package com.example.hibernateEmpExample.components;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="employeetable")
public class employeetable {
    @Id
    @Column(name = "eid")
private int eid;
    @Column(name = "ename")
private String ename;
private int sal;
private String dept;

    public employeetable() {
    }

    public employeetable(int eid, String ename, int sal, String dept) {
        this.eid = eid;
        this.ename = ename;
        this.sal = sal;
        this.dept = dept;
    }

    public int getEid() {
        return eid;
    }

    public void setEid(int eid) {
        this.eid = eid;
    }

    public String getEname() {
        return ename;
    }

    public void setEname(String ename) {
        this.ename = ename;
    }

    public int getSal() {
        return sal;
    }

    public void setSal(int sal) {
        this.sal = sal;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }
}
